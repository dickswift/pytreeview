# -*- coding: utf-8 -*-
"""\ 
This module reads syntactic trees encoded in CoNLL format.
It outputs objects of type TAGEntries so as to be displayed by pytreeview. 
Author: Yannick Parmentier 
Date: 2017/04/02
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, os, conllu
from grammar       import TAGEntry

class CoNLLloader(object):
    
    def __init__(self, conllfile, encoding):
        self.tbfile  = conllfile
        self.tbdir   = os.path.dirname(os.path.abspath(self.tbfile))
        self.tbext   = os.path.splitext(self.tbfile)[-1]
        self.tbext   = self.tbext[1:] #to remove the initial '.'
        self.encoding= encoding
        self.suffix  = 1

    def getGrammar(self, g, progload=None):
        try:
            import conllu.parser 
            trees = []
            # get trees
            with open(self.tbfile, 'r') as treefile:
                trees = conllu.parse_tree(treefile.read())
            treefile.close()
            # convert trees one by one
            nbSent= len(trees)
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbSent)
                else: #pyForms bar
                    progload.max = nbSent
                for sentence in trees:
                    tree = self.getEntry(sentence)  #tree here is a full syntactic tree
                    if progload is not None:
                        if 'printProgressBar' in dir(progload):
                            progload.value += 1
                            progload.printProgressBar()
                        else: #pyForms bar
                            progload.value += 1
                    g.addEntry(tree)
        except ImportError as e:
            #nltk not installed
            print(e)
            print('Please install python package conllu to load CoNLL-style treebanks')
            sys.exit(1)
                
    def getEntry(self,tbentry):
        tname      = 'TB_' + str(self.suffix)
        tfamily    = 'TB'
        ttrace     = []
        tsemantics = ''
        xmlfs      = None 
        tiface     = None 
        ttree      = Node(tbentry)
        self.suffix= self.suffix + 1
        return TAGEntry(tname,tfamily,ttrace,ttree,tsemantics,tiface)

class Node(object):

    def __init__(self, tbnode):
        self._color = 'black' #for tree diff
        self._ntype = 'lex'
        self._name  = ''
        label       = tbnode.token
        children    = tbnode.children #list to be converted into tuple later
        fs = []
        #print(label)
        for f,v in label.items():
            if f == 'form':
                self._name = v
            elif f == 'upostag':
                fs.append(('cat',v))
            elif f == 'feats':
                if v != None:
                    for key, val in v.items():
                        fs.append((key,val))
            else:
                if v is not None:
                    fs.append((f,v))
            self._fs = FS(fs)
        self._children = tuple(map(Node,children))
            
    def cat(self):
        nodeCat = ''
        try:
            nodeCat = str(self._fs.table['cat'])
        except KeyError:
            sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
            sys.stderr.flush()
        #print(str(self._fs) + ' ' + nodeCat) #debug only
        if nodeCat == '':
            nodeCat = '-'
        return nodeCat

    def typerepr(self):
        if self._ntype == None:
            return ''
        elif self._ntype in ['lex','flex']:
            return ''
        elif self._ntype == 'foot':
            return u'\u2605' #black star
        elif self._ntype in ['anc', 'anchor']:
            return u'\u2666' #black diamond
        elif self._ntype in ['coanc', 'coanchor']:
            return u'\u25c7' #white diamond
        elif self._ntype == 'subst':
            return u'\u2193' #downarrow
        elif self._ntype == 'nadj':
            return u'\u266f' #sharp sign
        elif self._ntype == 'std':
            return ''
        else:
            print('Unknow node type: ' + self._ntype)
            return ''
    
    def childCount(self):
        return len(self._children)

    @property
    def children(self):
        return self._children

    @property
    def color(self):
        return self._color
    
    def setColor(self, col):
        self._color = col

    def subColor(self, col):
        self._color = col
        for i in self._children:
            i.subColor(col)
        
    @property
    def ntype(self):
        return self._ntype

    @property
    def name(self):
        return self._name

    @property
    def fs(self):
        return self._fs

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        return "%s<%s>" % (self._name + ' ' + str(self._fs), repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            return "%s(%s)" % (self.cat(), repr_children)
        else:
            return "%s" % (self.cat())

class FS(object):

    def __init__(self, feats):
        self.coref = None
        self.table = {}
        for f,v in feats:
            self.table[f] = v

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in avm.table.keys():
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                elif isinstance(avm.table[k], int):
                    s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' #',' are used by split in tree2svg
                else: #should not happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + ':' + str(self.table[x]) + ','
        s +=']'
        return s
