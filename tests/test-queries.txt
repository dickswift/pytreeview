%Test file to be applied on tiger.json

#x:[cat="P"] ;0

#x:[word="hat"] ;1

#x:[morph=/Masc.*/] ;1

#x:[word="hat"] > #y ;0

#x:[pos={"ART"|"NN"}] & #y:[pos="NN"] & #x .* #y ;1

#x:[cat="NP"] > #y:[cat="V"] ;0

#x:[cat="NP"] > #y:[pos="ADV"] ;0

#x:[cat="NP"] >* #y:[pos="ADV"] ;1

#x:[cat="NP"] >2 #y:[pos="ADV"] ;0

#x:[cat="NP"] >2 #y:[pos="PIAT"] ;1

#x:[cat="NP"] >3 #y:[pos="ADV"] ;1

#x:[cat="NP"] >1,2 #y:[label="NK"] ;1

#x:[word="Die"] .* #y:[word="Teilnehmer"] ;1

#x:[label="SB"] .1 #y:[label="OA"] ;0

#x:[label="SB"] .2 #y:[label="OA"] ;1

#x:[word="als"] .1 #y:[word="zuvor"] ;0

#x:[word="als"] .2 #y:[word="zuvor"] ;1

#x:[word="als"] .1,2 #y:[word="zuvor"] ;1

#x:[word="als"] .2,3 #y:[word="zuvor"] ;1

#x:[word="als"] .3,4 #y:[word="zuvor"] ;0

#x:[cat="NP"] >@l #y:[word="als"] ;1

#x:[cat="NP"] >@r #y:[word="als"] ;0

#x:[cat="NP"] >@r #y:[word="zuvor"] ;1

#x:[label="SB"] $ #y:[label="OA"] ;1

#x:[label="SB"] $. #y:[label="OA"] ;0

#x:[label="SB"] $.* #y:[label="OA"] ;1

( #x:[cat={"AP"|"NP"}] > #y:[word="mehr"] | #x >* [cat="V"] ) ;1

#x:[cat={"AP"|"NP"}] > #y:[word="mehr"] & #x >* [cat="V"] ;0

#x:[cat="NP"] & ( #x > [word="toto"] | #x > [cat="AP"] ) ;1

( #x > [cat="AP"] | #x > [word="hat"] ) & #x:[cat="S"] ;1

( ( [cat={"S"|"V"}] > [word="hat"] | [cat="P"] >* [cat="V"] ) | #x:[word="toto"] ) ;1

( ( [cat={"P"|"V"}] > [word="hat"] | [cat="P"] >* [cat="V"] ) | #x:[word="toto"] ) ;0

( [cat={"P"|"V"}] > [cat="V"] | [cat="P"] >* [cat="V"] ) & #x:[cat="S"] ;0

#x:[cat="S"]  & #x > [cat="AP"] & #x >* [cat="AVP"] ;0

#x:[cat="S"]  & ( #x > [cat="AP"] | #x >* [cat="AVP"] ) ;1

( #x:[cat="S"] > #y:[cat="NP"] | #x:[cat="S"] > #y:[cat="V"] ) & ( #z:[label="NK"] . #v:[pos="NN"] | #v:[cat="V"] ) ;1

( #x:[cat="S"] > #y:[cat="NPP"] | #x:[cat="S"] > #y:[cat="V"] ) & ( #z:[label="NK"] . #v:[pos="NN"] | #v:[cat="V"] ) ;0

( #x:[cat="S"] > #y:[cat="NP"] | #x:[cat="S"] > #y:[cat="V"] ) & ( #z:[label="NK"] . #v:[pos="NK"] | #v:[cat="AP"] ) ;1
