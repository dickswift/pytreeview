# pytreeview

## description

Pytreeview is a lightweight tree-based grammar explorer.

Pytreeview expects as an input either a tree-based grammar in XML format which has been encoded using the following DTD: [xmg-tad.dtd,xml](https://sourcesup.renater.fr/xmg/xmg-tag.dtd,xml), or a treebank (in [FTB](http://ftb.linguist.univ-paris-diderot.fr/) XML format or in [PTB](https://catalog.ldc.upenn.edu/ldc99t42) text format).

When the [XMG2](http://dokufarm.phil.hhu.de/xmg/) system is installed and XMG2's *synsem* compiler for the [XMG](https://sourcesup.renater.fr/xmg/Documentation.pdf) domain-specific language ([DSL](https://en.wikipedia.org/wiki/Domain-specific_language)) meta-compiled, pytreeview can act as a wrapper for this *synsem* compiler (that is, it can take as an input a tree-based grammar description in the XMG language, compile it into XML and then explore it).

When used as a desktop application (GUI mode), one can highlight specific node features to display (by selecting them in the interface or declaring them -separated by semi-colons- in the 'Feats' textarea), or compare trees pairwise (by selecting two entries). 

Pytreeview offers tree conversion to PNG and [TIKZ](https://www.ctan.org/pkg/pgf) in both desktop or web application (NB: TIKZ tree representations do not contain features).

## Installation

### Requirements

To install ``pytreeview``'s requirements, we recommand you to use [pip](https://docs.python.org/3/installing/index.html) (and [virtualenv](https://docs.python.org/3/library/venv.html)). The easiest way to install ``pytreeview`` is to use the included ``requirements.txt`` file by invoking ``pip3 install -r requirements.txt``).

Please note that **svg2tikz** needs to be installed from its sources by invoking ``python3 setup.py install``:

* [svg2tikz](https://github.com/kjellmf/svg2tikz) (needed only for converting trees to latex, optional)

For your information, here are official webpages of ``pytreeview``'s requirements:

* [svgwrite](https://github.com/mozman/svgwrite) (needed for displaying trees)
* [cairosvg](http://cairosvg.org/) (needed for displaying trees)
* [wand](http://docs.wand-py.org/en/0.4.4/) (needed for displaying trees)
* [nltk](http://www.nltk.org/) (needed for processing ptb files)
* [pyforms](https://github.com/UmSenhorQualquer/pyforms) (needed for GUI mode)
* [lark-parser](https://github.com/erezsh/lark) (needed for searching through trees)
* [jsonpath_rw](https://github.com/kennknowles/python-jsonpath-rw) (needed for searching through trees)
* [conllu](https://github.com/EmilStenstrom/conllu) (needed only for processing conll files, optional)

**Please note that pytreeview has only been tested under Python3.x**.

Please note also that when using ``pytreeview`` in GUI mode, one may get the following message:
<pre>
QScintilla2 not installed
/usr/local/lib/python3.5/dist-packages/PyQt5/Qt/lib/libQt5Core.so.5: version `Qt_5.11' not found (required by /usr/local/lib/python3.5/dist-packages/PyQt5/Qsci.so)
Traceback (most recent call last):
  File "/usr/local/lib/python3.5/dist-packages/pyforms/controls.py", line 64, in <module>
    from pyforms.gui.controls.ControlCodeEditor import ControlCodeEditor
  File "/usr/local/lib/python3.5/dist-packages/pyforms/gui/controls/ControlCodeEditor.py", line 17, in <module>
    from PyQt5.Qsci import QsciScintilla, QsciLexerPython, QsciAPIs
ImportError: /usr/local/lib/python3.5/dist-packages/PyQt5/Qt/lib/libQt5Core.so.5: version `Qt_5.11' not found (required by /usr/local/lib/python3.5/dist-packages/PyQt5/Qsci.so)
</pre>

It indicates that some **optional** [pyforms](http://pyforms.readthedocs.io/en/latest/) requirements are missing. This is only a warning and **will not prevent pytreeview from working properly**.

### Install

Download or git clone pytreeview's source directory, then add ``path_to_sourcedir`` to your **PATH** environment variable. The ``pytreeview`` command can then be invoked from any location on your system.

## Usage

<code>usage: pytreeview [-h] [--mode [{GUI,WEB,CLI}]]
                  [-i INPUT | -l LOAD | -c COMPILE] [--save] [--version]
Tool for desktop or web display of tree grammars in XML format
optional arguments:
  -h, --help            show this help message and exit
  --mode [{GUI,WEB,CLI}]
                        viewer mode: GUI (default), CLI (for conversion
                        to binary format)or WEB
  -i INPUT, --input INPUT
                        specify input (meta)grammar file (.xmg | .xml)
  -l LOAD, --load LOAD  specify input binary grammar file (.pyt)
  -c COMPILE, --compile COMPILE
                        the metagrammar (.xmg or .mg file) needs to be
                        compiled first
  -t TREEBANK, --treebank TREEBANK
                        input trees are syntactic trees from an XML treebank
                        (FTB format)
  -p PTB, --ptb PTB     input trees are syntactic trees in PTB format
  -x XTAG, --xtag XTAG  input XTAG grammar directory
  -g TIGER, --tiger TIGER
                        input trees are syntactic trees in Tiger XML format
  -d DEPENDENCIES, --dependencies DEPENDENCIES
                        input trees are syntactic trees in CoNLL format
  --save                save input trees in binary format (.pyt)
  --dump                dump a json export of the tree collection
  --latex               create a set of latex files (one per tree)
  --maxsize MAXSIZE     maxsize of sentences to be considered for latex file
                        creation (default value is 15)
  --version             show program's version number and exit
</code>

## License

pytreeview is released under the terms of the GPL, see LICENSE.

## Acknowledgements

pytreeview reuses some extracts of code from [XMG2](https://github.com/spetitjean/XMG-2) by Jean-Baptiste Perrin and Denys Duchier, and from [xtag2xml](https://github.com/xmg-hhu/xtag2xml) by Esther Seyffarth (see source file headers for more details).
