# -*- coding: utf-8 -*-
"""\
This module contains the definition of subcat frames for
grammar extraction.
Author: Yannick Parmentier
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

class SubCat(object):

    def __init__(self, mCat):
        self._mother     = mCat #cat of the mother node
        self._frames     = {}   #dict size-frames (a frame is a concat. of syn categories separated by ';')
        self._frequencies= {}   #dict frame-freq
        
    @property
    def mother(self):
        return self._mother
    
    @property
    def frames(self):
        return self._frames

    @property
    def frequencies(self):
        return self._frequencies

    def addOccu(self, childCat):
        cCat    = ';'.join(childCat) #inner representation of cat frames (see above)
        # Update _frames (size <-> frames)
        if len(childCat) not in self._frames.keys():
            self._frames[len(childCat)] = [cCat]
        if cCat not in self._frames[len(childCat)]:
            self._frames[len(childCat)].append(cCat)
        # Update _frequencies (frame <-> nb of occurrences)
        if cCat in self._frequencies.keys():
            self._frequencies[cCat] = self._frequencies[cCat] + 1
        else:
            self._frequencies[cCat] = 1

    def getCandidateHead(self, headtable):
        s = ''
        threshold = 10 #we are only taking into account cats appearing at least 10 times
        catFreqs  = {}
        l = sorted(self._frequencies.keys(), key=self.getFreq, reverse=True)
        l2= (x for x in l if self.getFreq(x) >= 2) #we ignore frames appearing only once
        for frame in l2:
            #print(frame)
            freq = self._frequencies[frame]
            for cat in set(frame.split(';')):
                if cat in catFreqs.keys():
                    catFreqs[cat] += freq
                else:
                    catFreqs[cat]  = freq
        fcats = sorted(catFreqs.keys(), key=lambda x: catFreqs[x], reverse=True)
        #we update the headtable dict (for json export)
        headtable[self._mother] = [(x,catFreqs[x]) for x in fcats if catFreqs[x] >= threshold]
        s += '   Head candidates (cumulated frequencies) for ' + self._mother + ': \n'
        for fcat in fcats[:5]: #we print the 5 most frequent cats
            s += '      ' + fcat + '(' + str(catFreqs[fcat]) + ')\n'
        return s

    def __repr__(self):
        s = self._mother + ':\n'
        # Sort frames by size:
        for fsize in sorted(self._frames.keys()):
            # Within a size, sort frames by frequency (reverse order):
            l = sorted(self._frames[fsize], key=self.getFreq, reverse=True)
            for cat in l:
                if cat != '':
                    s+= '   ' + cat + ' (' + str(self._frequencies[cat]) + ')\n'
                else:
                    s+= '   ' + 'empty_cat' + ' (' + str(self._frequencies[cat]) + ')\n'
        return s

    def getFreq(self, cCat):
        return self._frequencies[cCat]

