# -*- coding: utf-8 -*-
"""\ 
This module reads tree grammars encoded in XTAG format.  
It is based on code by Esther Seyffarth for the xtag2xml utility
(see https://github.com/xmg-hhu/xtag2xml)
Author: Esther Seyffarth (courtesy of the author)
Date: 2016/04/22
"""
import re, os

class grammarTree:

    def __init__(self, definition, sourcefilename):
        self.family = os.path.basename(sourcefilename).split(".",1)[0]
        self.family = re.sub("\x03", "beta", re.sub("\x02", "alpha", self.family))
        self.name = definition.split('"',2)[1] #if '"' in definition else definition
        self.name = re.sub("\x03", "beta", re.sub("\x02", "alpha", self.name))
        self.nodes = self.getNodes(definition)
        self.hierarchy = self.getHierarchy(definition)

    def getNodes(self, definition):
        # find the "S_r.b" part in line "S_r.b:<inv> = -":
        node = re.compile("([A-Za-z_0-9]+(_r)?)\.((b|t))")
        nodes = {}
        for result in re.findall(node, definition):
            nodeName = result[0]
            topOrBottom = result[2]
            if nodeName in nodes:
                if not topOrBottom in nodes[nodeName]:
                    nodes[nodeName].update({topOrBottom: {}})
            else:
                nodes[nodeName] = {topOrBottom: {}}

        # differentiate between absolute feature values and values that
        # reference other nodes
        absoluteValue = re.compile("([A-Za-z_0-9]+(_r)?)\.(b|t):(<[a-z-]+>)\s*=\s*([^:\s]+)\s")
        relativeValue = re.compile("([A-Za-z_0-9]+(_r)?)\.(b|t):(<[a-z-]+>)\s*=\s*([A-Za-z_0-9]+(_r)?)\.(b|t):(<[a-z-]+>)")
        for result in re.findall(absoluteValue, definition):
            nodeName = result[0]
            topOrBottom = result[2]
            feature = result[3]
            featureValue = result[4]
            if '"' in featureValue:
                featureValue = featureValue.rsplit('"')[0]
            if not feature in nodes[nodeName][topOrBottom]:
                nodes[nodeName][topOrBottom][feature] = featureValue

        # map variable names like "@A" to dictionaries of nodes like "{node: {top: feature}}"
        self.variables = {}
        for result in re.findall(relativeValue, definition):
            nodeName = result[0]
            topOrBottom = result[2]
            feature = result[3]
            relatedNodeName = result[4]
            relatedNodeTopOrBottom = result[6]
            relatedNodeFeature = result[7]
            # find out if referenced node has an absolute value! if it does,
            # copy the value to this node.
            if relatedNodeFeature in nodes[relatedNodeName][relatedNodeTopOrBottom]:
                nodes[nodeName][topOrBottom][feature] = nodes[relatedNodeName][relatedNodeTopOrBottom][relatedNodeFeature]
            else:
                node1 = {nodeName: {topOrBottom: feature}}
                node2 = {relatedNodeName: {relatedNodeTopOrBottom: relatedNodeFeature}}
                found = False

                # check if current node info is already contained in one of the
                # variable dictionaries in self.variables
                featureAlreadyHasVariable = False
                for existingVariable in self.variables:
                    if nodeName in self.variables[existingVariable]:
                        if topOrBottom in self.variables[existingVariable][nodeName]:
                            if feature == self.variables[existingVariable][nodeName][topOrBottom]:
                                self.variables[existingVariable].update(node2)
                                featureAlreadyHasVariable = True
                    if relatedNodeName in self.variables[existingVariable]:
                        if relatedNodeTopOrBottom in self.variables[existingVariable][relatedNodeName]:
                            if relatedNodeFeature == self.variables[existingVariable][relatedNodeName][relatedNodeTopOrBottom]:
                                self.variables[existingVariable].update(node1)
                                featureAlreadyHasVariable = True


                if not featureAlreadyHasVariable:
                    for reference in self.variables:
                        newDict1 = self.variables[reference].copy()
                        newDict2 = self.variables[reference].copy()
                        newDict1.update(node1)
                        newDict2.update(node2)
                        if self.variables[reference] == newDict1:
                            if not self.variables[reference] == newDict2:
                                self.variables[reference].update(node2)
                                found = True
                        elif self.variables[reference] == newDict2:
                            if not self.variables[reference] == newDict1:
                                self.variables[reference].update(node1)
                                found = True
                    if not found:
                        addition = node1.copy()
                        addition.update(node2)
                        self.variables["@"+numberToString(len(self.variables))] = addition

        nodes = mergeDicts(nodes, self.variables)
        return(nodes)

    def getHierarchy(self, definition):
        # identify the part of the .trees file where the tree structure is explicitly encoded
        structure_notation = re.compile("\s(?=\(+\")")
        self.hierarchy = re.split(structure_notation, definition, 1)[-1]
        self.hierarchy = re.sub("\x06", "epsilon", self.hierarchy)
        self.hierarchy = bracketToDict(self.hierarchy, self.nodes)
        return self.hierarchy


########## Utility functions used above
def numberToString(number):
    """
    integer -> string
    Generate variable names (for cross-referenced node feature values).
    Variable 0 will be "A", Variable 1 will be "B", etc.
    Variable 26 will be "AA", Variable 27 will be "AB"
    """
    if number/26 < 1:
        correspondingString = chr(number+65)
        return correspondingString
    else:
        correspondingString = numberToString(int(number/26)-1) + \
                              numberToString(number%26)
        return correspondingString


def bracketToDict(bracket_string, node_feature_dict):
    """
    string, dictionary -> dictionary
    Parse the bracket representation of the trees from the *.trees files.
    Return a dictionary that represents the tree.
    Each node has the attributes name (node name), features (as given in
    node_feature_dict) and children (list of nodes).
    """

    # in this case, bracket_strings are read as
    # "((bracket A) dominates (bracket B) and (bracket C))"

    result_dict = {}

    # remove surrounding brackets that have no value
    #surrounding_brackets = re.compile("^\s*\(([^()]*)\)\s*$")
    surrounding_brackets = re.compile("^\s*\((.*)\)\s*$")
    bracket_string = re.sub(surrounding_brackets, r"\1", bracket_string)

    # cleaning up node names in bracket_string
    simple_nodename = re.compile("\"([A-Za-z_0-9]+)\"\s*\.\s*\"\s*\"")
    compound_nodename = re.compile("\"([A-Za-z_0-9]+)\"\s*\.\s*\"([A-Za-z_0-9]+)\"")
    bracket_string = re.sub(simple_nodename, r"\1", bracket_string)
    bracket_string = re.sub(compound_nodename, r"\1_\2", bracket_string)

    # sticking type information to the relevant node, e.g. :substp T, :footp T
    type_info = re.compile("(\s*):([a-z]*)\s*T")
    bracket_string = re.sub(type_info, r"#\2", bracket_string) # to keep Type info

    # sticking connector information to the relevant node, e.g. :connector :LINE
    conn_info = re.compile("(\s*):connector\s*:(LINE)")
    bracket_string = re.sub(conn_info, r"+\2", bracket_string) # to keep connector info
    #bracket_string = re.sub(conn_info, r"", bracket_string) # to discard connector info

    # sticking display-feature information to the relevant node
    display_feature_info = re.compile("(\s*):(display)-feature\?\s*T")
    bracket_string = re.sub(display_feature_info, r"!\2", bracket_string)

    # sticking constraints info to relevant node, e.g. :constraints "NA"
    constraints_info = re.compile("(\s*):constraints\s+\"([^\"]*)\"\s*")
    bracket_string = re.sub(constraints_info, r"%\2", bracket_string)

    # sticking constraint-type info to the relevant node, e.g. :constraint-type :NA
    constraint_type_info = re.compile("(\s*):constraint-type\s+:([^\(\)]*)\s*")
    bracket_string = re.sub(constraint_type_info, r"$\2", bracket_string)

    # cleaning up bracket structure of bracket_string;
    # remove brackets that enclose a single node
    useless_brackets = re.compile("\(([A-Za-z_0-9#\+!%$\"]+\s*)\)")
    while re.search(useless_brackets, bracket_string):
        bracket_string = re.sub(useless_brackets, r"\1", bracket_string)

    # easy case: no brackets left in the string
    final_nodename = re.compile("[A-Za-z_0-9#\+!%$]+\s*")
    node_name_no_info = re.compile("^[^#\+!%$\s]+")
    if not "(" in bracket_string and not ")" in bracket_string:
        nodes = re.findall(final_nodename, bracket_string)
        node_name_only = re.findall(node_name_no_info, nodes[0])[0]
        if node_name_only in node_feature_dict.keys():
            node_features = node_feature_dict[node_name_only]
        else:
            node_features = {}

        result_dict = {"name": nodes[0].rstrip("#+!%$ "), "children": [], "features": node_features}
        for daughter in nodes[1:]:
            daughter_name_only = re.findall(node_name_no_info, daughter)[0]
            if daughter_name_only in node_feature_dict.keys():
                daughter_features = node_feature_dict[daughter_name_only]
            else:
                daughter_features = {}
            result_dict["children"].append({"name": daughter.rstrip("#+!%$ "), "children": [], "features": daughter_features})
        return result_dict

    # hard case: still brackets left - call this function again
    else:
        # first node definitely dominates everything that comes after
        root = re.findall(final_nodename, bracket_string)[0]
        rest = bracket_string.replace(root, "")

        # divide rest into list of siblings (daughters to root)
        children = identify_siblings(rest, final_nodename)

        first_level_children = []
        for child in children:
            first_level_children.append(bracketToDict(child, node_feature_dict))
        root_name_only = re.findall(node_name_no_info, root)[0]
        if root_name_only in node_feature_dict.keys():
            root_features = node_feature_dict[root_name_only]
        else:
            root_features = {}

        result_dict = {"name": root.rstrip("#+!%$ "), "children":first_level_children, "features": root_features}
        return result_dict


def identify_siblings(string, final_nodename):
    """
    string, string -> list
    Split up given string (part of a bracket structure from a *.trees file)
    into a list of sibling node brackets.
    Return a list of strings.
    Ex. input string:   "V#headp  (PP_1 (P_1 to )  NP_1#substp )"
    Ex. output list:    ['V#headp  ', '(PP_1 (P_1 to )  NP_1#substp )'].
    """
    complex_siblings = []
    bracket_levels = 0
    sibling_start_index = None
    siblings = []

    # find start and end index of each sibling
    for index, char in enumerate(string):
        if char == "(":
            if bracket_levels == 0:
                sibling_start_index = index
            bracket_levels += 1

        elif char == ")":
            bracket_levels -= 1
            if bracket_levels == 0:
                complex_siblings.append((sibling_start_index, index))

    # generate list of all siblings (simple or bracketed)
    prev_index = 0
    for start, end in complex_siblings:
        for atomar_sibling in re.findall(final_nodename, string[prev_index:start]):
            siblings.append(atomar_sibling)
        siblings.append(string[start:end+1])
        prev_index = end+1
    # add anything that is located to the right of the last bracketed sibling - this
    # will always be another sibling, never a daughter node
    for atomar_sibling in re.findall(final_nodename, string[complex_siblings[-1][1]+1:]):
        siblings.append(atomar_sibling)

    return siblings


def mergeDicts(absoluteValues, variableMappings):
    """
    dict, dict -> dict
    Generate a dictionary that contains the absolute feature values as well
    as feature values that are variable names.
    """
    mergedDict = absoluteValues.copy()
    for variable in variableMappings:
        for node in variableMappings[variable]:
            for position in variableMappings[variable][node]:
                feature = variableMappings[variable][node][position]
                if node in absoluteValues:
                    if position in absoluteValues[node]:
                        if feature in absoluteValues[node][position]:
                            mergedDict[node][position].update({feature: variable})
                        else:
                            mergedDict[node][position][feature] = variable
                    else:
                        mergedDict[node][position] = {feature: variable}
                else:
                    mergedDict[node]= {position: {feature: variable}}

    return mergedDict


