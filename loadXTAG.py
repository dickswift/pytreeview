# -*- coding: utf-8 -*-
"""\ 
This module reads tree grammars encoded in XTAG format.  
It is based on code by Esther Seyffarth for the xtag2xml utility
(see https://github.com/xmg-hhu/xtag2xml)
Author: Yannick Parmentier 
Date: 2017/02/15
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os, sys, re
from grammar  import TAGEntry
from xtag     import grammarTree

# Function used to avoid FutureWarning on
# split on empty pattern
# cf http://stackoverflow.com/questions/29988595/python-regex-splitting-on-pattern-match-that-is-an-empty-string
def megasplit(pattern, string):
    splits = list((m.start(), m.end()) for m in re.finditer(pattern, string))
    starts = [0] + [i[1] for i in splits]
    ends = [i[0] for i in splits] + [len(string)]
    return [string[start:end] for start, end in zip(starts, ends)]

class XTAGloader(object):
    
    def __init__(self, filename, encoding):
        self.xtagfile = filename
        self.encoding = encoding

    def getGrammar(self, g, progload):
        infile = open(self.xtagfile, "r", encoding=self.encoding)
        doc = infile.read()
        # identify all trees in this document
        # replaced with megasplit to avoid FutureWarning
        #treeDefinition = re.compile("(^|\s)(?=\(\")")
        #doc1 = re.split(treeDefinition,doc)
        doc1 = megasplit("(^|\s)(?=\(\")",doc)
        # update progress bar
        nbtrees = len(doc1)
        if progload is not None:
            if 'printProgressBar' in dir(progload):
                progload.setTotal(nbtrees)
            else:
                progload.max = nbtrees
        # extract trees with features
        for treeDef in doc1:
            treeDef = treeDef.strip()
            if treeDef:
                try:
                    newTree = grammarTree(treeDef, self.xtagfile)
                    pytree  = self.getEntry(newTree)
                    if progload is not None:
                        progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                    g.addEntry(pytree)
                except TypeError as e:
                    print(e, self.xtagfile)

    def getEntry(self, xtagtree):
        tname      = xtagtree.name
        tfamily    = xtagtree.family
        ttrace     = []
        tsemantics = ''
        tiface     = None
        ttree      = Node(xtagtree.hierarchy)
        return TAGEntry(tname,tfamily,ttrace,ttree,tsemantics,tiface)

        
class Node(object):

    def __init__(self, xtagnode):
        self._color = 'black' #for tree diff        
        self._ntype = 'std'   #default value
        self._name  = ''
        if len(xtagnode["features"])==0 and len(xtagnode["children"])==0:
            self._ntype = 'lex'
        self._name = xtagnode["name"].replace('!display','')
        # extract info from node name
        catVal = self._name.split('#',1)[0].split('%',1)[0]
        if '#' in self._name:
            typeVal= self._name.split('#',1)[1].split('%',1)[0]
            if typeVal == 'footp':
                self._ntype = 'foot'
            elif typeVal == 'substp':
                self._ntype = 'subst'
            elif typeVal == 'headp':
                self._ntype = 'anc'
        else:
            if '%' in self._name:
                const = self._name.split('%',1)[1]
                if const == 'NA$NA':
                    self._ntype = 'nadj'
        # process features
        self._fs = FS(xtagnode["features"])
        # add cat in features
        self.fs.addCat(catVal)
        self._children = tuple(map(Node,xtagnode["children"]))
        
    def cat(self):
        nodeCat = ''
        if self._fs != None:
            try:
                nodeCat = str(self._fs.table['cat'])
            except KeyError:
                sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
                sys.stderr.flush()
        return nodeCat

    @property
    def children(self):
        return self._children

    @property
    def color(self):
        return self._color
    
    def setColor(self, col):
        self._color = col

    def subColor(self, col):
        self._color = col
        for i in self._children:
            i.subColor(col)
        
    @property
    def ntype(self):
        return self._ntype

    @property
    def name(self):
        return self._name

    @property
    def fs(self):
        return self._fs

    def typerepr(self):
        if self._ntype == None:
            return ''
        elif self._ntype in ['lex','flex']:
            return ''
        elif self._ntype == 'foot':
            return u'\u2605' #black star
        elif self._ntype in ['anc', 'anchor']:
            return u'\u2666' #black diamond
        elif self._ntype in ['coanc', 'coanchor']:
            return u'\u25c7' #white diamond
        elif self._ntype == 'subst':
            return u'\u2193' #downarrow
        elif self._ntype == 'nadj':
            return u'\u266f' #sharp sign
        elif self._ntype == 'std':
            return ''
        else: #should not happen
            print('Unknow node type: ' + self._ntype)
            return ''
    
    def maxChildFeatSize(self): #unused
        if len(self._children) > 0:
            return max([len(n.fs.prettyprint(n.fs)) for n in self._children])
        else:
            return 0
    
    def childCount(self):
        return len(self._children)

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        return "%s<%s>" % (self._name + ' ' + str(self._fs), repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            return "%s(%s)" % (self.cat(), repr_children)
        else:
            return "%s" % (self.cat())

        
class FS(object):

    def __init__(self, xtagfs):
        self.coref = None
        self.table = {}
        for feat in xtagfs.keys():
            if feat == "t":
                self.table["top"] = FS(xtagfs["t"])
            elif feat == "b":
                self.table["bot"] = FS(xtagfs["b"])
            else:
                self.table[clean(feat)] = xtagfs[feat] if '/' not in xtagfs[feat] else ALT(xtagfs[feat])

    def addCat(self, catValue):
        self.table['cat'] = catValue

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in sorted(avm.table.keys(), reverse=True):
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                elif isinstance(avm.table[k], ALT):
                    s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' #';' are used by split in tree2svg
                else: #should never happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + ':' + str(self.table[x]) + ','
        s +=']'
        return s

def clean(featname):
    return str(featname[1:-1]) #to remove leading and trailing < >

class ALT(object):

    def __init__(self, alt):
        self.coref = None
        self.values = tuple(alt.split('/'))

    def __repr__(self):
        s = '{'
        first = True
        for x in self.values:
            if not(first):
                s += '|' + x
            else:
                s += x
                first = False
        s +='}'
        return s
