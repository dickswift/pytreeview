# -*- coding: utf-8 -*-
"""\ 
This module reads tree grammars encoded in XML format (see DTD at
https://sourcesup.renater.fr/xmg/xmg-tag.dtd,xml) and returns a
TAGrammar object composed of TAGEntries.  It reuses extracts of code
from XMG2's treeview by Jean-Baptiste Perrin and Denys Duchier.
Author: Yannick Parmentier 
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
from grammar  import TAGEntry
from lxml     import etree

class XMLloader(object):
    
    def __init__(self, filename, encoding):
        self.xmlfile = filename
        self.encoding= encoding

    def getGrammar(self, g, progload):
        parser    = etree.XMLParser(remove_comments=True, encoding=self.encoding)
        try:
            xmldoc   = etree.parse(self.xmlfile, parser = parser)
            nbentries= xmldoc.xpath('count(//entry)') #len(xmldoc.xpath("/grammar/entry"))
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbentries)
                else:
                    progload.max = nbentries
            for entry in xmldoc.xpath("/grammar/entry"):
                tage = self.getEntry(entry)
                if progload is not None:
                    progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                g.addEntry(tage)
        except etree.XMLSyntaxError:
            raise Exception("XML syntax error")

    def getEntry(self,xmlentry):
        tname      = xmlentry.get('name')
        tfamily    = xmlentry.find('family').text
        ttrace     = []
        for classe in xmlentry.find('trace').iter('class'):
            ttrace.append(classe.text)
        #tsemantics = gensem(xmlentry.find('semantics', None))
        xmlsem     = xmlentry.find('semantics', None)
        tsemantics = None if xmlsem is None else list(map(Sem, xmlsem.findall('literal')))        
        xmlfs      = xmlentry.find('interface/fs', None)
        tiface     = None if xmlfs is None else FS(xmlfs)
        ttree      = Node(xmlentry.find('tree/node'))
        return TAGEntry(tname,tfamily,ttrace,ttree,tsemantics,tiface)

class Node(object):

    def __init__(self, xmlnode):
        self._color = 'black' #for tree diff        
        self._ntype = xmlnode.get("type",None)
        self._name  = xmlnode.get("name",None)
        xmlfs = xmlnode.find("narg/fs", None)
        self._fs = None if xmlfs is None else FS(xmlfs)
        self._children = tuple(map(Node,xmlnode.findall("node")))
        
    def cat(self):
        nodeCat = ''
        if self._fs != None:
            try:
                nodeCat = str(self._fs.table['cat'])
            except KeyError:
                sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
                sys.stderr.flush()
        return nodeCat

    @property
    def children(self):
        return self._children

    @property
    def color(self):
        return self._color
    
    def setColor(self, col):
        self._color = col

    def subColor(self, col):
        self._color = col
        for i in self._children:
            i.subColor(col)
        
    @property
    def ntype(self):
        return self._ntype

    @property
    def name(self):
        return self._name

    @property
    def fs(self):
        return self._fs

    def typerepr(self):
        if self._ntype == None:
            return ''
        elif self._ntype in ['lex','flex']:
            return ''
        elif self._ntype == 'foot':
            return u'\u2605' #black star
        elif self._ntype in ['anc', 'anchor']:
            return u'\u2666' #black diamond
        elif self._ntype in ['coanc', 'coanchor']:
            return u'\u25c7' #white diamond
        elif self._ntype == 'subst':
            return u'\u2193' #downarrow
        elif self._ntype == 'nadj':
            return u'\u266f' #sharp sign
        elif self._ntype == 'std':
            return ''
        else: #should not happen
            print('Unknow node type: ' + self._ntype)
            return ''
    
    def maxChildFeatSize(self): #unused
        if len(self._children) > 0:
            return max([len(n.fs.prettyprint(n.fs)) for n in self._children])
        else:
            return 0
    
    def childCount(self):
        return len(self._children)

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        fs  = '' if self._fs is None else str(self._fs)
        name= '' if self._name is None else self._name
        return "%s<%s>" % (name + ' ' + fs, repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            return "%s(%s)" % (self.cat(), repr_children)
        else:
            return "%s" % (self.cat())

        
class FS(object):

    def __init__(self, xmlfs):
        self.coref = xmlfs.get("coref", None)
        self.table = {}
        for xmlf in xmlfs.findall("f"):
            if xmlf.find('*', None) is None:
                self.table[xmlf.get("name")] = xmlf.get("varname")
            else:
                self.table[xmlf.get("name")] = feature_value(xmlf.find("*"))

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in avm.table.keys():
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                elif isinstance(avm.table[k], ALT):
                    s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' #';' are used by split in tree2svg
                else: #should never happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + ':' + str(self.table[x]) + ','
        s +=']'
        return s
    
def feature_value(value):
    tag = value.tag
    assert tag in ("fs","sym","vAlt")
    if value.tag == "fs":
        return FS(value)    
    if value.tag == "sym":
        if not value.get('varname') is None:
            return value.get('varname')
        else:
            return value.get("value")
    if value.tag == "vAlt":
        return ALT(value)

class ALT(object):

    def __init__(self, alt):
        self.coref = alt.get("coref", None)
        self.values = tuple(map(feature_value,alt.findall("sym")))

    def __eq__(self,  other):
        if str(other).find('ALT') > -1:
            s = ''
            for i in self.values:
                s += i + '|'
            y = ''
            for i in other.values:
                y += i + '|'
            return s == y
        else:
            return False

    def __repr__(self):
        s = '{'
        first = True
        for x in self.values:
            if not(first):
                s += '|' + x
            else:
                s += x
                first = False
        s +='}'
        return s
        
class Sem(object):

    def __init__(self, semlit):
        self.neg   = False
        self.label = None
        self.pred  = None
        self.args  = []
        if semlit.get("negated") != 'no':
            self.neg = True
        lab = semlit.find("label", None)
        if lab is not None:
            self.label = gensem(lab)
        pred = semlit.find("predicate")
        if pred is not None:
            self.pred = gensem(pred)
        for arg in semlit.findall("arg"):
            self.args.append(gensem(arg))        

    def __eq__(self,  other):
        return str(self) == str(other)

    def __repr__(self):
        s = ''
        if self.neg:
            s += "!"
        if self.label is not None:
            s += self.label + ':'
        if self.pred is not None:
            s += self.pred
            s += "("
        for a in self.args:
            s += a + ','
        if len(s) > 0 and s[len(s)-1] == ',':
            s = s[:-1]
            s+=')'
        return s


def gensem(i):
    s = ''
    if 'value' in i.find('sym').attrib:
        s+= i.find('sym').get('value')
    elif 'varname' in i.find('sym').attrib: 
        s+= i.find('sym').get('varname')
    return s

