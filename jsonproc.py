# -*- coding: utf-8 -*-
"""\
This module contains the processing of jsonpath querys on trees.
Author: Yannick Parmentier
Date: 2017/05/11
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, re, json
from jsonpath_rw           import parse
from queryproc             import QueryProcessor

class JSONPathSearch(object):

    def __init__(self, constraint_sets, entries=None):
        self._pattern = '$.tree..fs'      #'$.entries[*].tree..fs' #basic pattern for jsonpath queries (meaning 'any node')
        self._accu    = constraint_sets   #sets of nodes (each one coming with structural contraints)
        self._entries = entries           #list of dicts (one dict per entry)


    def processQuery(self):
        for e in self._entries:
            if self.processQueryEntry(e):
                yield e

        
    def processQueryEntry(self, entry=None):
        #process a search query against an entry
        keep         = False      #default: no cset matches, if we find one, we stop
        alternatives = self._accu #when disjunctions are used, there is more than one set (_accu is a list of dict)
        csetIndex    = 0
        while csetIndex < len(alternatives) and not(keep):
            constraint_set = alternatives[csetIndex]
            #print(constraint_set)#debug only
            cset_ok  = True       #default: this constraint_set matches
            nodedefs = list(constraint_set.values())
            nodeIndex= 0
            while nodeIndex < len(nodedefs) and cset_ok : #several nodes may be used in a set
                nodedef = nodedefs[nodeIndex]
                #print(nodedef) #debug only
                feats    = nodedef.feats
                dom      = nodedef.dom
                largedom = nodedef.largedom
                prec     = nodedef.prec
                largeprec= nodedef.largeprec
                domn     = nodedef.domn
                dommn    = nodedef.dommn
                precn    = nodedef.precn
                precmn   = nodedef.precmn
                sibling  = nodedef.sibling
                sister   = nodedef.sister
                sibprec  = nodedef.sibprec
                leftmost = nodedef.leftmost
                rightmost= nodedef.rightmost
                #1. check features (and keep paths leading to valid nodes)
                keeppaths,_ = self.processFeatQuery(feats, entry, self._pattern)
                #print(keeppaths) #debug only
                nb_valid = 0
                if len(keeppaths) > 0:
                    #2. check node relations (for each candidate n1 node)
                    for p in keeppaths:
                        #print(p) #debug only
                        if dom:
                            _,rel_ok = self.processRelation('dom', dom, p, constraint_set, entry)
                            if not(rel_ok):
                                #dom failure
                                continue #check next candidate
                        if largedom:
                            _,rel_ok = self.processRelation('largedom', largedom, p, constraint_set, entry)
                            if not(rel_ok):
                                #largedom failure
                                continue
                        if prec:
                            _,rel_ok = self.processRelation('prec', prec, p, constraint_set, entry)
                            if not(rel_ok):
                                #prec failure
                                continue
                        if largeprec:
                            _,rel_ok = self.processRelation('largeprec', largeprec, p, constraint_set, entry)
                            if not(rel_ok):
                                #largeprec failure
                                continue
                        if domn:
                            _,rel_ok = self.processRelation('domn', domn, p, constraint_set, entry)
                            if not(rel_ok):
                                #domn failure
                                continue
                        if dommn:
                            _,rel_ok = self.processRelation('dommn', dommn, p, constraint_set, entry)
                            if not(rel_ok):
                                #dommn failure
                                continue
                        if precn:
                            _,rel_ok = self.processRelation('precn', precn, p, constraint_set, entry)
                            if not(rel_ok):
                                #precn failure
                                continue
                        if precmn:
                            _,rel_ok = self.processRelation('precmn', precmn, p, constraint_set, entry)
                            if not(rel_ok):
                                #precmn failure
                                continue
                        if sibling:
                            _,rel_ok = self.processRelation('sibling', sibling, p, constraint_set, entry)
                            if not(rel_ok):
                                #sibling failure
                                continue
                        if sister:
                            _,rel_ok = self.processRelation('sister', sister, p, constraint_set, entry)
                            if not(rel_ok):
                                #sister failure
                                continue
                        if sibprec:
                            _,rel_ok = self.processRelation('sibprec', sibprec, p, constraint_set, entry)
                            if not(rel_ok):
                                #sibprec failure
                                continue
                        if leftmost:
                            _,rel_ok = self.processRelation('leftmost', leftmost, p, constraint_set, entry)
                            if not(rel_ok):
                                #leftmost failure
                                continue
                        if rightmost:
                            _,rel_ok = self.processRelation('rightmost', rightmost, p, constraint_set, entry)
                            if not(rel_ok):
                                #rightmost failure
                                continue                        
                        #if we get here, we have a solution
                        nb_valid += 1
                else:
                    #feature failure (no candidate node)
                    cset_ok = False
                    break
                if nb_valid == 0:
                    cset_ok = False
                    break
                nodeIndex += 1
            keep = keep or cset_ok #as soon as one c_set matches, the entry is kept
            csetIndex += 1
        return keep

    
    def processFeatQuery(self, feats, entry=None, prefix=None):
        #get candidate nodes for a given set of feature constraints
        keep     = True
        keeppaths=[]
        for k,v in feats.items():
            #print("*" + prefix)  #debug only
            jsonquery = prefix + '.' + k
            #get expected values as a list
            if isinstance(v,list):
                expected =  v
            else:
                expected = [v]
            if entry != None: #actual search
                #print("**" + jsonquery)  #debug only
                jpath_expr = parse(jsonquery)
                #get the json nodes (paths) referring to the feature under consideration
                nodes      = [(str(match.full_path),str(match.value)) for match in jpath_expr.find(entry)]
                #print(nodes)             #debug only
                #check the feature values and keep the compatible feature paths
                #case A: the expected values are constants
                #case B: expected values can be REGEXP
                solutions  = ['$.'+x for x,y in nodes if self.match(y, expected)]
                #print('***' + str(solutions))         #debug only
                if len(solutions) == 0: #in case of no compatible feature, discard entry
                    #print('Unsatisfied feature constraint: ' + str(feats), file=sys.stderr)
                    keep = False
                    break
                else:
                    keeppaths.extend(solutions)
        return (keeppaths, keep)


    def match(self, featval, expvalues):
        res = False #default value
        i   = 0
        while i < len(expvalues) and not(res):
            e = expvalues[i]
            if e[0]=='/': #regexp
                prog = re.compile(e[1:-1])
                result = prog.search(featval)
                #print(featval)
                #print(result)
                res = res or (result != None)
            else:         #constant
                res = res or (featval.lower() == e.lower())
            i += 1
        return res


    def processRelation(self, reltype, relvalues, path, nodes, entry=None):
        #getting candidate nodes for a given type of relations (paths consistency)
        keep     = True
        keeppaths=[]
        for (_,n2,n3) in relvalues: #n2 is a node identifier
            solutions=[]
            paths    =[]
            path2    =path #init
            #print('*' + path)            
            if reltype == 'dom':
                path2 += '.`parent`.`parent`.children.[*].fs'  #parent-parent to move up (cat-fs)
                paths = [path2]
            elif reltype == 'largedom':
                path2 += '.`parent`.`parent`.children.[*]..fs'
                paths = [path2]                
            elif reltype == 'prec':
                splits     = re.findall('\d+|\D+', path)
                pathA      = ''.join(map(lambda s : '*' if s.isnumeric() else s, splits))
                jpath      = parse(pathA)
                paths      = [str(match.full_path) for match in jpath.find(entry)]
                #get the first nodepath which has the same length as path, and which appears after path
                #in the depth-first traversal
                found = False
                pathB = None
                for pp in paths:
                    if found and len('$.'+pp)==len(path):
                        pathB = pp + '.`parent`'
                        break
                    if ('$.'+pp)==path:
                        found=True
                if pathB != None:
                    paths=[pathB]
            elif reltype == 'largeprec':
                splits     = re.findall('\d+|\D+', path)
                pathA      = ''.join(map(lambda s : '*' if s.isnumeric() else s, splits))
                jpath      = parse(pathA)
                paths      = [str(match.full_path) for match in jpath.find(entry)]
                #print(paths) #debug only
                found = False
                pathB = ''
                for pp in paths:
                    if found and len('$.'+pp)==len(path):
                        pathB += pp + '.`parent`' + '|'
                    if ('$.'+pp)==path:
                        found=True
                if pathB != '':
                    pathB = pathB[:-1] #to remove the final "|"
                    paths = pathB.split('|')
            elif reltype == 'domn':
                n = n3
                pathB = '.`parent`.`parent`.children.[*]'
                pathC = ''
                for i in range(1,n):
                    pathC += '.children.[*]'
                path2 += pathB + pathC + '.fs'
                paths = [path2]
            elif reltype == 'dommn':
                m,n = n3  #interval [m,n]
                pathB = '.`parent`.`parent`.children.[*]'
                pathC = ''
                pathD = ''
                for i in range(1,n+1):
                    if i >= m:
                        pathD += path2 + pathB + pathC + '.fs'
                        if i < n:
                            pathD += '|'
                    pathC += '.children.[*]'
                if pathD != '':
                    paths = pathD.split('|')
            elif reltype == 'precn':
                n = n3
                splits     = re.findall('\d+|\D+', path)
                pathA      = ''.join(map(lambda s : '*' if s.isnumeric() else s, splits))
                jpath      = parse(pathA)
                paths      = [str(match.full_path) for match in jpath.find(entry)]
                #print(paths) #debug only
                found = False
                pathB = None
                #we keep the nth path
                for pp in paths:
                    if found and len('$.'+pp)==len(path):
                        if n > 1:  #1 and not 0 cause we are at the occurrence following "path"
                            n -= 1
                        else: 
                            pathB = pp + '.`parent`'
                            break
                    if ('$.'+pp)==path:
                        found=True
                if pathB != None:
                    paths = [pathB]                
            elif reltype == 'precmn':
                m,n = n3
                splits     = re.findall('\d+|\D+', path)
                pathA      = ''.join(map(lambda s : '*' if s.isnumeric() else s, splits))
                jpath      = parse(pathA)
                paths      = [str(match.full_path) for match in jpath.find(entry)]
                #print(paths) #debug only
                found = False
                pathB = None
                #we keep the nth path
                for pp in paths:
                    if found and len('$.'+pp)==len(path):
                        if m > 1:
                            m -= 1
                        else:
                            pathB = pp + '.`parent`' + '|'
                            if n < n3[1]:
                                break
                            n -= 1
                    if ('$.'+pp)==path:
                        found=True
                if pathB != None:
                    pathB = pathB[:-1] #to remove the final "|"
                    paths = pathB.split('|')
            elif reltype == 'sibling':
                regexp = re.match(r'(.*)(?<=\D)(\d+)', path) #we get the last number in the path
                if regexp is not None: #None appears in case of root (there is no child number)
                    nodepos = regexp.group(2) 
                    #print(nodepos)
                    pathA = path + '.`parent`.`parent`.`parent`.[:' + nodepos +'].fs'
                    pathB = path + '.`parent`.`parent`.`parent`.[' + str(int(nodepos)+1) +':].fs'
                    path2 = pathA + '|' + pathB
                else:
                    path2 = None
                if path2 != None:
                    paths = path2.split('|')
            elif reltype == 'sister':
                regexp = re.match(r'(.*)(?<=\D)(\d+)', path)
                if regexp is not None: #case of root (there is no child number)
                    nodepos = regexp.group(2) #last number
                    #print(nodepos)
                    path2 += '.`parent`.`parent`.`parent`.[' + str(int(nodepos)+1) +'].fs'
                else:
                    path2 = None
                if path2 != None:
                    paths = [path2]
            elif reltype == 'sibprec':
                regexp = re.match(r'(.*)(?<=\D)(\d+)', path)
                if regexp is not None: 
                    nodepos = regexp.group(2) 
                    path2 += '.`parent`.`parent`.`parent`.[' + str(int(nodepos)+1) + ':].fs'
                else:
                    path2 = None
                if path2 != None:
                    paths = [path2]
            elif reltype == 'leftmost':
                #print(path)
                pathA      = path2 + '.`parent`.`parent`.children.[*]..fs'
                jpath      = parse(pathA)
                lpaths     = [str(match.full_path) for match in jpath.find(entry)]
                #print(lpaths) #debug only
                if len(lpaths) > 0:
                    maxlen     = len(max(lpaths,key=len))
                    path2      = [x for x in lpaths if len(x)==maxlen][0]
                #print(path2)
                if len(path2) > 0:
                    paths = [path2]
            elif reltype == 'rightmost':
                #print(path)
                pathA      = path2 + '.`parent`.`parent`.children.[*]..fs'
                jpath      = parse(pathA)
                rpaths     = [str(match.full_path) for match in jpath.find(entry)]
                #print(rpaths) #debug only
                if len(rpaths) > 0:
                    maxlen     = len(max(rpaths,key=len))
                    path2      = [x for x in rpaths if len(x)==maxlen][-1]
                #print(path2)
                if len(path2) > 0:
                    paths = [path2]
            results  = False #default                    
            allpaths = (x for x in paths)
            for apath in allpaths:
                solutions,_ = self.processFeatQuery(nodes[n2].feats, entry, apath)
                valid_apath = len(solutions)!=0
                results     = results or valid_apath
                if valid_apath:
                    keeppaths.extend(solutions)
                    break
            if not results:
                #print('Unsatisfied node relation constraint: ' + reltype + ' ' + str(relvalues) + ' ' + str(nodes[n2]), file=sys.stderr)
                keep=False
                break
        return (keeppaths, keep)

                    
if __name__ == "__main__":
    if len(sys.argv) == 2:
        with open(sys.argv[1]) as f:
            d = json.load(f)
        query = input('Query ? ')
        while query != 'q':
            q = QueryProcessor()
            try:
                q.parseQuery(query)
                accu = q.accu
                #print(q.iast)
                #print(accu)
                j = JSONPathSearch(accu, d['entries'])
                for t in j.processQuery():
                    print(t)
            except Exception as e:
                print('Ill-formed query, please check your query', file=sys.stderr)
                print(e)
                raise e
            query = input('Query ? ')
    elif len(sys.argv) == 3: #unit test
        with open(sys.argv[1]) as f:
            d = json.load(f)
        with open(sys.argv[2]) as g:
            queries = g.readlines()
        for qu in queries:
            if len(qu.strip()) > 0 and qu[0] != '%':
                instruction = qu.split(';')
                query       = instruction[0]
                result      = int(instruction[1].strip())
                qproc       = QueryProcessor()
                print('Processing ' + query + '...', end='', file=sys.stderr)
                qproc.parseQuery(query)
                accu = qproc.accu
                #print(qproc.iast)
                #print(accu)
                j   = JSONPathSearch(accu, d['entries'])
                res = [x for x in j.processQuery()]
                try:
                    assert(len(res)==result)
                    print(' OK', file=sys.stderr)
                except AssertionError as e:
                    print(' *** ERROR ***', file=sys.stderr)
                
